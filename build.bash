#!/bin/bash

# usage: TARGET=LPC1768 TOOLCHAIN=GCC_ARM ./build.bash

# Goes into each example directory under "examples" and sets it up as an
# mbed application, linking back to the mbed-os and wdwalker-mbed
# directories in this directory as needed. Note that a link will only be
# made if the file doesn't already exist.
#
# After the links are made in each directory, compiles using mbed.

target=${TARGET:-LPC1768}
toolchain=${TOOLCHAIN:-GCC_ARM}
dirs=${1:-`find examples -name main.cpp | sed -e "s^/main.cpp^^"`}

declare -a FILES=(.mbed .mbedignore mbed_app.json mbed-os.lib mbed-os wdwalker-mbed.lib wdwalker-mbed)

startdir=`pwd`
for dir in $dirs; do
    echo "*************************************************"
    echo "Working in" $dir
    cd $dir
    for f in "${FILES[@]}"; do
	[ -e $f ] || ln -fs $startdir/$f
    done
    cmd="mbed compile --build BUILD/$target/$toolchain --source . -m $target -t $toolchain"
    echo $cmd
    $cmd
    cd $startdir
done
