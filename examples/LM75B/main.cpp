#include <wdwalker-mbed/peripherals/LM75B.h>
#include <wdwalker-mbed/peripherals/C12832.h>

using namespace wdwalker::mbed::peripherals;

LM75B sensor(I2C_SDA_PIN, I2C_SCL_PIN);
C12832 lcd(LCD_MOSI, LCD_SCK, LCD_RESET, LCD_A0, LCD_NCS);

int main () {
    char data[2];
    if (!sensor.test_connection()) {
        lcd.locate(0, 0);
        lcd.printf("LM75B not found on I2C");
        lcd.paint();
    } else {
        while (1) {
            lcd.clear();
            lcd.locate(0,3);
            lcd.printf("Temp = %.1fC / %.1fF\n", sensor.temp_c(), sensor.temp_f());
            lcd.paint();
            wait(1.0);
        }
    }
}
