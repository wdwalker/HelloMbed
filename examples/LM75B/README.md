# LM75B

## Summary
LM75B is a trivial use of the [LM75B](https://wdwalker.github.io/wdwalker-mbed/classwdwalker_1_1mbed_1_1peripherals_1_1_l_m75_b.html) utility class to read from the I2C-based LM75B temperature sensor on the [LPC1768 application board](https://developer.mbed.org/cookbook/mbed-application-board).

If you followed the 'Downloading and Building' instructions in the [top-level README.md](../../README.md), you will already have a binary you can copy to your device:

```
cp BUILD/LPC1768/GCC_ARM/LM75B.bin /Volumes/MBED
```

If you make any changes to main.cpp and want to rebuild, just type the following command while sitting in this directory:

```
mbed compile && cp BUILD/LPC1768/GCC_ARM/LM75B.bin /Volumes/MBED
```

## What to Expect
The LCD on the application board will display the current temperature in Celsius and Fahrenheit.

## See Also
* [LM75B Data Sheet](http://www.nxp.com/documents/data_sheet/LM75B.pdf)
* [main.cpp](main.cpp)

