# Joystick

## Summary
Joystick is a trivial use of [DigitalIn](https://docs.mbed.com/docs/mbed-os-api-reference/en/latest/APIs/io/DigitalIn/) and [wait](https://docs.mbed.com/docs/mbed-os-api-reference/en/latest/APIs/tasks/wait/) to read the values of the joystick on the joystick on the [LPC1768 application board](https://developer.mbed.org/cookbook/mbed-application-board). It then uses [DigitalOut](https://docs.mbed.com/docs/mbed-os-api-reference/en/latest/APIs/io/DigitalOut/) to light up LEDs according to which button was pressed.

If you followed the 'Downloading and Building' instructions in the [top-level README.md](../../README.md), you will already have a binary you can copy to your device:

```
cp BUILD/LPC1768/GCC_ARM/joystick.bin /Volumes/MBED
```

If you make any changes to main.cpp and want to rebuild, just type the following command while sitting in this directory:

```
mbed compile && cp BUILD/LPC1768/GCC_ARM/joystick.bin /Volumes/MBED
```

## What to Expect
As you move the joystick around, different LEDs will light up on the board. If you press the center button, all the LEDs will light up.

## See Also
* [main.cpp](main.cpp)

