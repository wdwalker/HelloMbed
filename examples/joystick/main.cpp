#import <mbed.h>

int main() {
    DigitalIn down(p12);
    DigitalIn left(p13);
    DigitalIn center(p14);
    DigitalIn up(p15);
    DigitalIn right(p16);

    DigitalOut led1(LED1);
    DigitalOut led2(LED2);
    DigitalOut led3(LED3);
    DigitalOut led4(LED4);

    while (true) {
        int all = center.read();
        led1 = center || down.read();
        led2 = center || left.read();
        led3 = center || up.read();
        led4 = center || right.read();
        wait_ms(10);
    }
}
