#include <mbed.h>

// LED1 is an alias for the PinName that's connected to
// the first LED on most development boards. If you want
// to use something else, such as p16 on the LPC1768,
// just substitute that PinName for LED1.
DigitalOut led1(LED1);

int main() {
    while (true) {
        led1 = !led1;
        wait_ms(500);
    }
}

