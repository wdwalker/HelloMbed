# Blinky

## Summary
Blinky is a trivial use of [DigitalOut](https://docs.mbed.com/docs/mbed-os-api-reference/en/latest/APIs/io/DigitalOut/) and [wait](https://docs.mbed.com/docs/mbed-os-api-reference/en/latest/APIs/tasks/wait/) to blink an LED.

If you followed the 'Downloading and Building' instructions in the [top-level README.md](../../README.md), you will already have a binary you can copy to your device:

```
cp BUILD/LPC1768/GCC_ARM/blinky.bin /Volumes/MBED
```

If you make any changes to main.cpp and want to rebuild, just type the following command while sitting in this directory:

```
mbed compile && cp BUILD/LPC1768/GCC_ARM/blinky.bin /Volumes/MBED
```

## What to Expect
LED1 on the board will blink over and over.

## Details
Nearly every mbed development board has at least one addressable LED on a pin named ```LED1```. The blinky app just creates a DigitalOut instance that refers to this and then alternates between taking it high and low. Forever.

If you want to drive your own LED on a breadboard, you can do so quite easily. Just pick out which pin on of the development board that you want to use (e.g., "p14") and connect it to an [LED circuit](https://en.wikipedia.org/wiki/LED_circuit):

* The pin you choose on the development connects to the anode side of the LED (the side with the longer wire)
* The cathode side of the LED (the side with the flat spot and shorter wire) connects to a resistor
* The other side of the resistor connects to ground

Choosing the resistor value is relatively simple if you have the data sheet on your LED (if you don't 330 ohm is fine). The data sheet will tell you the forward voltage (Vf) and the intended operating current (I). Given these along with the source voltage coming out of your pin (Vs, typically 3.3v), the resistor value should be:

R = (Vs - Vf) / I

For example, given a [typical red LED](https://www.sparkfun.com/datasheets/Components/LED/COM-09590-YSL-R531R3D-D2.pdf):

* Vs = 3.3V (from the dev board datasheet)
* Vf ~= 2.0V (from the LED datasheet)
* I ~= 17mA (from the LED datasheet)

We compute R as follows:

* R = (Vs - Vf) / I
* R = (3.3 - 2.0) / 17mA
* R = (3.3 - 2.0) / 0.017
* R = 76 ohm

So, we round up and a 100 ohm resistor will do; it will just give us a lower current. Using I = V/R, we get:

* I = (Vs - Vf) / R
* I = (3.3 - 2.0) / 100
* I = 0.013 = 13mA

A tad under the suggested current, but fine. In fact, you can go very high - 10k ohm - and still see the LED turn on and off. The 20ma from the datasheet is the absolute maximum rating for sustained current.

## See Also
* Wikipedia [LED circuit article](https://en.wikipedia.org/wiki/LED_circuit)
* sparkfun [LED tutorial](https://learn.sparkfun.com/tutorials/light-emitting-diodes-leds?_ga=1.238079616.1201963036.1481377610)
* [LED resistor calculator](http://www.ohmslawcalculator.com/led-resistor-calculator)
* [main.cpp](main.cpp)

