# C12832

## Summary
C12832 uses [C12832](https://wdwalker.github.io/wdwalker-mbed/classwdwalker_1_1mbed_1_1peripherals_1_1_c12832.html) to communicate with the [Newhaven Display C12832 128x32 LCD](http://www.newhavendisplay.com/specs/NHD-C12832A1Z-FSW-FBW-3V3.pdf) over SPI on the [LPC1768 application board](https://developer.mbed.org/cookbook/mbed-application-board).

If you followed the 'Downloading and Building' instructions in the [top-level README.md](../../README.md), you will already have a binary you can copy to your device:

```
cp BUILD/LPC1768/GCC_ARM/C12832.bin /Volumes/MBED
```

If you make any changes to main.cpp and want to rebuild, just type the following command while sitting in this directory:

```
mbed compile && cp BUILD/LPC1768/GCC_ARM/C12832.bin /Volumes/MBED
```

## What to Expect
The LCD on the application board will run through a variety of tests.

## See Also
* [Newhaven Display NHD-C12832A1Z-FSW-FBW-3V3 Data Sheet](http://www.newhavendisplay.com/specs/NHD-C12832A1Z-FSW-FBW-3V3.pdf)
* [Sitronix ST7565R 65 x 132 Dot Matrix LCD Controller/Driver Data Sheet](http://www.newhavendisplay.com/app_notes/ST7565R.pdf)
* [main.cpp](main.cpp)

