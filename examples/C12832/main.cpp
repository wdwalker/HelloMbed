#include <wdwalker-mbed/peripherals/C12832.h>
#include "Arial12x12.h"
#include "Arial28x28.h"

using namespace wdwalker::mbed::peripherals;

int main() {
    C12832 lcd(LCD_MOSI, LCD_SCK, LCD_RESET, LCD_A0, LCD_NCS);
    lcd.clear();

#if 1
    // Draw a straight line across the top using pixels
    for (int x = 0; x < lcd.width(); x++) {
        lcd.pixel(x, 0);
    }

    // Draw straight lines down the right, across the bottom,
    // and up the left sides.
    lcd.line(0, 0, 0, lcd.height() - 1);
    lcd.line(0, lcd.height() - 1, lcd.width() - 1, lcd.height() - 1);
    lcd.line(lcd.width() - 1, lcd.height() - 1, lcd.width() - 1, 0);

    // Draw an X on the left hand side
    lcd.line(0, 0, lcd.height() - 1, lcd.height() - 1);
    lcd.line(lcd.height() - 1, 0, 0, lcd.height() - 1);

    // Draw an unfilled circle on the left
    lcd.circle(lcd.height() / 2 - 1, lcd.height() / 2 - 1, 10);

    // Draw an unfilled rectangle and a filled rectangle
    lcd.rectangle(30, 10, 50, 22);
    lcd.rectangle(77, 10, 97, 22, true);

    // Draw an X on the right hand side
    lcd.line(lcd.width() - 1, 0, lcd.width() - lcd.height(), lcd.height() - 1);
    lcd.line(lcd.width() - lcd.height(), 0, lcd.width() - 1, lcd.height() - 1);

    // Draw a filled circle on the right hand side
    lcd.circle(lcd.width() - lcd.height() / 2, lcd.height() / 2 - 1, 10, true);

    // Draw text in various ways
    lcd.character('m', 52, 17);
    lcd.character('b', -1, -1);
    lcd.character('e', -1, -1);
    lcd.character('d', -1, -1);
    lcd.locate(52, 8);
    lcd.printf("Hey!");

    // Finally - update the display.
    lcd.paint();
#endif

#if 1
    // Test changing the font
    wait(2.0);
    lcd.clear();
    lcd.set_font((unsigned char *) Arial12x12);
    lcd.locate(0, 0);
    lcd.printf("Arial12x12");
    lcd.paint();
#endif

#if 1
    // Test changing to a larger font
    wait(2.0);
    lcd.clear();
    lcd.locate(0, 0);
    lcd.set_font((unsigned char *) Arial28x28);
    lcd.printf("Arial28x28");
    lcd.paint();
#endif

#if 1
    // "Scroll" by setting the RAM top line
    wait(2.0);
    for (char i = 0; i < 0x40; i++) {
        wait(0.1);
        lcd.start_line(i);
    }
    lcd.start_line(0);
#endif

#if 1
    // Test command to set all the points on
    wait(2.0);
    for (int i = 0; i < 10; i++) {
        wait(0.2);
        lcd.all_points(true);
        wait(0.2);
        lcd.all_points(false);
    }
#endif

#if 1
    // Test changing the contrast
    wait(2.0);
    for (char i = 0; i < 0x40; i++) {
        wait(0.1);
        lcd.contrast(i);
    }
    lcd.contrast(0x27);
#endif

}
