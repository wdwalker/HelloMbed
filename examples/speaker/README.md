# Potentiometer

## Summary
Speaker is a trivial use of [AnalogIn](https://docs.mbed.com/docs/mbed-os-api-reference/en/latest/APIs/io/AnalogIn/) and [PwmOut](https://docs.mbed.com/docs/mbed-os-api-reference/en/latest/APIs/io/PwmOut/) to play musical notes on the speaker. It uses pot1 on the [LPC1768 application board](https://developer.mbed.org/cookbook/mbed-application-board) and controls the speaker on the same board via PWM.

If you followed the 'Downloading and Building' instructions in the [top-level README.md](../../README.md), you will already have a binary you can copy to your device:

```
cp BUILD/LPC1768/GCC_ARM/speaker.bin /Volumes/MBED
```

If you make any changes to main.cpp and want to rebuild, just type the following command while sitting in this directory:

```
mbed compile && cp BUILD/LPC1768/GCC_ARM/speaker.bin /Volumes/MBED
```

## What to Expect
As you turn pot1, the musical note played on the speaker will change.

## Details
All we are doing here is generating a perfect square wave (50% duty cycle) while changing the PWM period.

## See Also
* [main.cpp](main.cpp)

