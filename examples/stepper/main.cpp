#include <wdwalker-mbed/stepper/StepperMotor.h>

using namespace wdwalker::mbed::stepper;

/**
 * The fastest we want to go (microseconds/step). If this is too small,
 * the motor will block.
 */
const uint32_t MIN_SLEEP_US = 1000;

/**
 * The slowest we want to go (microseconds/step).
 */
const uint32_t MAX_SLEEP_US = 5000;

/**
 * Number of steps per revolution when in HALF_STEP mode. When in
 * WAVE_DRIVE or FULL_STEP mode, the number of steps per revolution
 * will be half this number.
 */
const int32_t STEPS_PER_REVOLUTION = 4096;

StepperMotor motor(IN1, IN2, IN3, IN4);

DigitalOut led1(LED1);
DigitalOut led2(LED2);
DigitalOut led3(LED3);
DigitalOut led4(LED4);

int main()
{
    led1 = 1;
    motor.step(STEPS_PER_REVOLUTION / 2, MIN_SLEEP_US);
    led1 = 0;

    led2 = 1;
    motor.step(-1 * STEPS_PER_REVOLUTION / 2, MIN_SLEEP_US);
    led2 = 0;

    led3 = 1;
    motor.step_smooth(STEPS_PER_REVOLUTION / 2, MIN_SLEEP_US, MAX_SLEEP_US,
                      STEPS_PER_REVOLUTION / 16);
    led3 = 0;

    led4 = 1;
    motor.step_smooth(-1 * STEPS_PER_REVOLUTION / 2, MIN_SLEEP_US, MAX_SLEEP_US,
                      STEPS_PER_REVOLUTION / 16);
    led4 = 0;

    motor.energize(false);
    deepsleep();
}
