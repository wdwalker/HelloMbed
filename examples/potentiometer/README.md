# Potentiometer

## Summary
Potentiometer is a trivial use of [AnalogIn](https://docs.mbed.com/docs/mbed-os-api-reference/en/latest/APIs/io/AnalogIn/) and [wait](https://docs.mbed.com/docs/mbed-os-api-reference/en/latest/APIs/tasks/wait/) to read the values of a potentiometer. It uses pot1 and pot2 on the [LPC1768 application board](https://developer.mbed.org/cookbook/mbed-application-board) and also hijacks p16 so you can hook up your own external potentiometer if you want to.

The potentiometer values are posted on the C12832 128x32 LCD display of the application board.


If you followed the 'Downloading and Building' instructions in the [top-level README.md](../../README.md), you will already have a binary you can copy to your device:

```
cp BUILD/LPC1768/GCC_ARM/potentiometer.bin /Volumes/MBED
```

If you make any changes to main.cpp and want to rebuild, just type the following command while sitting in this directory:

```
mbed compile && cp BUILD/LPC1768/GCC_ARM/potentiometer.bin /Volumes/MBED
```

## What to Expect
As you turn the potentiometers, their value will be updated on the C12832 LCD display.

## Details
In this example, the potentiometers are used as [voltage dividers](https://en.wikipedia.org/wiki/Voltage_divider). One side of the potentiometer is connected to ground, the other is connected to the 3.3v regulated output (Vout) of the LPC1768. The middle of the potentiometer (the "wiper") is connected to the pin assigned as an analog in.

>Note: the LPC1768 Vout is used as the Vin for the potentiometer circuit. From this point on, Vin refers to the voltage coming into the potentiometer from the Vout pin of the LPC1768. Furthermore, we will use Vout to stand for the output voltage coming from the potentiometer wiper.

When turned all the way in one direction, the output voltage at the potentiometer wiper is 0v. When turned all the way in the other direction, the output voltage is equal to the input voltage (Vin = 3.3v).

AnalogIn will convert this to a range from 0.0 to 1.0, where 0.0 maps to a voltage of 0.0v and 1.0 maps to a voltage of 3.3v.

The voltage divider math is easy:

Vout = Vin * R2 / (R1 + R2)

In our case:

* Vout = the voltage at the potentiometer wiper
* Vin = 3.3v
* R1 = the resistance between Vin and the wiper
* R2 = the resistance between the wiper and ground
* R1 + R2 = the total resistance of the potentiometer (e.g., a 10K Ohm pot).

So, when R1 is 0 Ohm (pot is turned all the way one way):

* R2 = 10K Ohm
* Vout = Vin = 3.3v
* AnalogIn reads 1.0

When R2 is 0 Ohm (pot is turned all the way the other way):

* Vout = 0v
* AnalogIn reads 0.0

When the pot is turned halfway:

* R1 = R2
* R2 / (R1 + R2) = 1/2
* Vout = Vin/2 = 3.3v/2 = 1.65v
* AnalogIn reads 0.5

## See Also
* Wikipedia [potentiometer article](https://en.wikipedia.org/wiki/Potentiometer)
* Wikipedia [voltage divider article](https://en.wikipedia.org/wiki/Voltage_divider)
* sparkfun [voltage dividers tutorial](https://learn.sparkfun.com/tutorials/voltage-dividers)
* [main.cpp](main.cpp)

