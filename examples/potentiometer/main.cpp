#include <wdwalker-mbed/peripherals/C12832.h>

using namespace wdwalker::mbed::peripherals;

int main()
{
    C12832 lcd(LCD_MOSI, LCD_SCK, LCD_RESET, LCD_A0, LCD_NCS);
    AnalogIn pot1(p19);
    AnalogIn pot2(p20);
    AnalogIn pot3(p16);
    while (true) {
        lcd.clear();
        lcd.locate(0, 0);
        lcd.printf("Pot1: %f", pot1.read());
        lcd.locate(0, 9);
        lcd.printf("Pot2: %f", pot2.read());
        lcd.locate(0, 18);
        lcd.printf("Pot3: %f", pot3.read());
        lcd.paint();
        wait_ms(500);
    }
}
