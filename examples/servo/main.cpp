#include <mbed.h>

// NOTE: You need an external power supply to power the servos.
// They will not run on USB power alone.

// Using http://www.micropik.com/PDF/SG90Servo.pdf, we see:
// * Ground = brown
// * Vcc = red
// * PWM = orange
// * 20ms PWM period
// * 1-2ms duty cycle - (5% to 10%)
//   * 1.0ms pulse (5.00%) is full left (-90 degrees)
//   * 1.5ms pulse (7.50%) is the middle (0 degrees)
//   * 2.0ms pulse (10.0%) is full right (90 degrees)

const float PWM_PERIOD = 0.020;
const float MIN_PULSE = 0.001; // You actually can go lower than this.
const float MAX_PULSE = 0.002; // You actually can go higher than this.

float duty_cycle(float pot) {
    return (MIN_PULSE + pot * (MAX_PULSE - MIN_PULSE)) / PWM_PERIOD;
}

int main() {
    AnalogIn pot1(p19);
    AnalogIn pot2(p20);
    PwmOut servo1(p21);
    PwmOut servo2(p22);

    // Note - according to the docs, there's really only one PWM_PERIOD
    // shared across all the PWMs, so these two lines are redundant.
    servo1.period(PWM_PERIOD);
    servo2.period(PWM_PERIOD);

    while (true) {
        servo1.write(duty_cycle(pot1.read()));
        servo2.write(duty_cycle(pot2.read()));
        wait_ms(10);
    }
}
