# Servo

## Summary
Servo is a trivial use of [PwmOut](https://docs.mbed.com/docs/mbed-os-api-reference/en/latest/APIs/io/PwmOut/) to control two [SG90](http://www.micropik.com/PDF/SG90Servo.pdf) servos. It uses pot1 and pot2 on the [LPC1768 application board](https://developer.mbed.org/cookbook/mbed-application-board) to set the positions of the servos: when you turn a potentiometer, the application changes the duty cycle of the associated servo accordingly.

>NOTE: You need to plug an external power supply into the application board to power the servos. They will not run on USB power alone.

If you followed the 'Downloading and Building' instructions in the [top-level README.md](../../README.md), you will already have a binary you can copy to your device:

```
cp BUILD/LPC1768/GCC_ARM/servo.bin /Volumes/MBED
```

If you make any changes to main.cpp and want to rebuild, just type the following command while sitting in this directory:

```
mbed compile && cp BUILD/LPC1768/GCC_ARM/servo.bin /Volumes/MBED
```

## What to Expect
As you turn each potentiometer, its associated servo will turn to match the potentiometer value.

## Details
In the [SG90 Data Sheet](http://www.micropik.com/PDF/SG90Servo.pdf), we see:

* Ground = brown wire
* Vcc = red wire (needs to be ~5v)
* PWM signal = orange wire
* 20ms PWM period
* 1-2ms duty cycle - (5% to 10% duty cycle)
  * 1.0ms pulse (5.00% duty) is full left (-90 degrees)
  * 1.5ms pulse (7.50% duty) is the middle (0 degrees)
  * 2.0ms pulse (10.0% duty) is full right (90 degrees)

Using this in the code, we set the servo.period to 0.020s (20ms). We then read the value of the potentiometer and use servo.write to set the duty cycle of the servo.

> NOTE: The data sheet says the duty cycle values are approximate. With the exact values above, you may find a smaller range of motion.

## See Also
* [SG90 Servo Data Sheet](http://www.micropik.com/PDF/SG90Servo.pdf)
* [main.cpp](main.cpp)
