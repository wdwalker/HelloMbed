#import <mbed.h>

// NOTE: The RGB LED is common anode, so that "0" is on, and "1" is off.

int main() {
    DigitalIn down(p12);
    DigitalIn left(p13);
    DigitalIn center(p14);
    DigitalIn right(p16);

    DigitalOut red(p23);
    DigitalOut blue(p24);
    DigitalOut green(p25);

    while (true) {
        int all = center.read();
        red = !(center || down.read());
        green = !(center || left.read());
        blue = !(center || right.read());
        wait_ms(10);
    }
}
