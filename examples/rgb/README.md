# RGB

## Summary
RGB is a trivial use of [DigitalOut](https://docs.mbed.com/docs/mbed-os-api-reference/en/latest/APIs/io/DigitalOut/) (to turn the LEDs on and off), [DigitalIn](https://docs.mbed.com/docs/mbed-os-api-reference/en/latest/APIs/io/DigitalIn/) (to read the joystick), and [wait](https://docs.mbed.com/docs/mbed-os-api-reference/en/latest/APIs/tasks/wait/) to blink the red, blue, and green colors of the RGB LED on the [LPC1768 application board](https://developer.mbed.org/cookbook/mbed-application-board).

>One thing you will see different from the blinky example is that the RGB LED is common anode. As a result, "0" is on, and "1" is off.

If you followed the 'Downloading and Building' instructions in the [top-level README.md](../../README.md), you will already have a binary you can copy to your device:

```
cp BUILD/LPC1768/GCC_ARM/rgb.bin /Volumes/MBED
```

If you make any changes to main.cpp and want to rebuild, just type the following command while sitting in this directory:

```
mbed compile && cp BUILD/LPC1768/GCC_ARM/rgb.bin /Volumes/MBED
```

## What to Expect
As you move the joystick, the different colors of the LED will light up. If you press the center button, all the colors will light up.

## See Also
* [Cree® PLCC4 3-in-1 SMD LED CLV1A-FKB Data Sheet](http://www.cree.com/led-components/media/documents/CLV1AFKB(874).pdf)
* [main.cpp](main.cpp)

