#include <wdwalker-mbed/peripherals/MMA7660.h>
#include <wdwalker-mbed/peripherals/C12832.h>

using namespace wdwalker::mbed::peripherals;

MMA7660 mma(I2C_SDA_PIN, I2C_SCL_PIN);
C12832 lcd(LCD_MOSI, LCD_SCK, LCD_RESET, LCD_A0, LCD_NCS);

const char *ORIENTATION[] = { "UP   ", "DOWN ", "RIGHT", "LEFT ", "BACK ", "FRONT", "UNKNOWN"
                            };

int main() {
    lcd.clear();
    if (!mma.test_connection()) {
        lcd.locate(0, 0);
        lcd.printf("MMA7660 not found on I2C");
        lcd.paint();
    } else {
        while (1) {
            lcd.locate(0, 0);
            lcd.printf("x: %f", mma.x());
            lcd.locate(0, 9);
            lcd.printf("y: %f", mma.y());
            lcd.locate(0, 18);
            lcd.printf("z: %f", mma.z());
            lcd.locate(58,0);
            lcd.printf("BaFro: %s", ORIENTATION[mma.get_side()]);
            lcd.locate(58,9);
            lcd.printf("PoLa:  %s", ORIENTATION[mma.get_orientation()]);
            //TODO: tap and shake need more work in the MMA7660 library
            //lcd.locate(58, 18);
            //lcd.printf("Tap:%d  Shake:%d", mma.get_tap(), mma.get_shake());
            lcd.paint();
            wait(0.25);
        }
    }
}
