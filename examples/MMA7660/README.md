# MMA7660

## Summary
MMA7660 is a trivial use of the [MMA7660](https://wdwalker.github.io/wdwalker-mbed/classwdwalker_1_1mbed_1_1peripherals_1_1_m_m_a7660.html) utility class to read from the I2C-based MMA7660 3-Axis Orientation/Motion Detection Sensor on the  [LPC1768 application board](https://developer.mbed.org/cookbook/mbed-application-board).

If you followed the 'Downloading and Building' instructions in the [top-level README.md](../../README.md), you will already have a binary you can copy to your device:

```
cp BUILD/LPC1768/GCC_ARM/MMA7660.bin /Volumes/MBED
```

If you make any changes to main.cpp and want to rebuild, just type the following command while sitting in this directory:

```
mbed compile && cp BUILD/LPC1768/GCC_ARM/MMA7660.bin /Volumes/MBED
```

## What to Expect
The LCD on the application board will display several readings from the MMA7660.

## See Also
* [MMA7660 Data Sheet](http://www.nxp.com/assets/documents/data/en/data-sheets/MMA7660FC.pdf)
* [main.cpp](main.cpp)

