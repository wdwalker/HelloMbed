[![Build Status](https://travis-ci.org/wdwalker/HelloMbed.svg?branch=master)](https://travis-ci.org/wdwalker/HelloMbed)
# HelloMbed
## Overview
This repository provides one-stop shopping for a number of examples working with the [mbed LPC1768](https://developer.mbed.org/platforms/mbed-LPC1768/) and its associated [application board](https://developer.mbed.org/cookbook/mbed-application-board).

Rather than downloading a bunch of separate repos just to get trivial ```main.cpp``` files less than 100 lines long, they are all here in one spot:

* [Blinky](examples/blinky/README.md): uses [DigitalOut](https://docs.mbed.com/docs/mbed-os-api-reference/en/latest/APIs/io/DigitalOut/) and [wait](https://docs.mbed.com/docs/mbed-os-api-reference/en/latest/APIs/tasks/wait/) to blink an LED
* [Potentiometer](examples/potentiometer/README.md): uses [AnalogIn](https://docs.mbed.com/docs/mbed-os-api-reference/en/latest/APIs/io/AnalogIn/) to read the values of a potentiometer
* [Joystick](examples/joystick/README.md): uses [DigitalIn](https://docs.mbed.com/docs/mbed-os-api-reference/en/latest/APIs/io/DigitalIn/) to read the values of a joystick
* [Servo](examples/servo/README.md): uses [PwmOut](https://docs.mbed.com/docs/mbed-os-api-reference/en/latest/APIs/io/PwmOut/) to control an [SG90](http://www.micropik.com/PDF/SG90Servo.pdf) servo
* [RGB](examples/rgb/README.md): uses [DigitalIn](https://docs.mbed.com/docs/mbed-os-api-reference/en/latest/APIs/io/DigitalIn/) and [DigitalOut](https://docs.mbed.com/docs/mbed-os-api-reference/en/latest/APIs/io/DigitalOut/) to turn on and off the red, blue, and green LEDs of the RGB LED on the [LPC1768 application board](https://developer.mbed.org/cookbook/mbed-application-board)
* [Speaker](examples/speaker/README.md): uses [AnalogIn](https://docs.mbed.com/docs/mbed-os-api-reference/en/latest/APIs/io/AnalogIn/) to read the value of a potentiometer to control which musical note to play on the speaker using [PwmOut](https://docs.mbed.com/docs/mbed-os-api-reference/en/latest/APIs/io/PwmOut/)
* (TODO) NPN Transistor:
* (TODO) IR Transmitter/Receiver:
* (TODO) Hall Effect
* (TODO) Capacitive Touch
* (TODO) 3.5mm Audio Jack: analog out
* (TODO) 3.5mm Audio Jack: analog in
* (TODO) USB Device: act as keyboard or mouse
* [C12832](examples/C12832/README.md): uses [C12832](https://wdwalker.github.io/wdwalker-mbed/classwdwalker_1_1mbed_1_1peripherals_1_1_c12832.html) to communicate with the [Newhaven Display C12832 128x32 LCD](http://www.newhavendisplay.com/specs/NHD-C12832A1Z-FSW-FBW-3V3.pdf) over SPI
* [MMA7660](examples/MMA7660/README.md): uses [MMA7660](https://wdwalker.github.io/wdwalker-mbed/classwdwalker_1_1mbed_1_1peripherals_1_1_m_m_a7660.html) to communicate with the [NXP MMA7660 3-Axis Orientation/Motion Detection Sensor](http://www.nxp.com/assets/documents/data/en/data-sheets/MMA7660FC.pdf) over I2C
* [LM75B](examples/LM75B/README.md): uses [LM75B](https://wdwalker.github.io/wdwalker-mbed/classwdwalker_1_1mbed_1_1peripherals_1_1_l_m75_b.html) to communicate with the [NXP LM75B digital temperature sensor and thermal watchdog](http://www.nxp.com/documents/data_sheet/LM75B.pdf) over I2C
* (TODO) Zigbee (Xbee):
* (TODO) WiFi (RN-XV):
* (TODO) RJ45 Ethernet: [with TLS]
* (TODO) USB mass storage:

## Prerequisites

These examples have been designed to work offline using the [mbed CLI](https://docs.mbed.com/docs/mbed-os-handbook/en/latest/dev_tools/cli/) and the [GNU ARM Embedded Toolchain](https://developer.arm.com/open-source/gnu-toolchain/gnu-rm). By running the 'mbed export' command in each example directory, one can also set up projects that can be imported into [GNU ARM Eclipse](http://gnuarmeclipse.github.io).

The main development platform is Mac OS X. Neither Linux nor Windows has been tested.

Many of the examples also use the [wdwalker-mbed](https://github.com/wdwalker/wdwalker-mbed) libraries - these are very simple libraries that build on top of the [mbed OS API](https://docs.mbed.com/docs/mbed-os-api-reference/en/latest/). These include things such as code to talk to I2C and SPI devices that live on the application board.

## Downloading and Building
Use the mbed CLI 'import' command to download the sources and then run ```./build.bash``` to do the initial build:

```
mbed import git@github.com:wdwalker/HelloMbed
cd HelloMbed
./build.bash
```

```./build.bash``` does the following:

* Sets up each example directory to point to the mbed sources and config files that were imported by the mbed CLI
* Compiles each example into a build directory called ```BUILD/LPC1768/GCC_ARM``` under each example. The resulting ```*.bin``` file in each build directory will be named after the example. You can copy this file to the mbed LPC1768 (/Volumes/MBED), press the reset button, and watch it work.

After this step, you can go into each example and type ```mbed compile``` to recompile and create a fresh *.bin file to copy to your mbed.

If you want to build just one example, pass its directory name to ```build.bash```. For example:

```
./build.bash examples/servo
```

## See Also
* [mbed Application Board Data Schematic](https://developer.mbed.org/media/uploads/chris/mbed-014.1_b.pdf)
* [mbed Application Board Cookbook](https://developer.mbed.org/cookbook/mbed-application-board) - lots of very good demos here
* [mbed Application Board Hands-On Demos](https://developer.mbed.org/users/4180_1/notebook/mbed-application-board-hands-on-demos/) for some other fun things to do
* Repos from [Chris Styles](https://developer.mbed.org/users/chris/code/) - these served as a good starting point for a number of the demos
